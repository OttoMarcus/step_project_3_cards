const span = document.querySelector(".copyright");

export default function getDate() {
    let date = new Date();
    span.innerText = date.getFullYear();
}