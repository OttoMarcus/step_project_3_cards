class Modal {
    constructor() {
    //   this.modal = document.querySelector(".modal");
    //   this.openModalBtn = document.getElementById("openModalBtn");
    //   this.closeBtns = document.querySelectorAll(".closeBtn");
    //   this.createBtn = document.getElementById("createBtn");
      this.additionalFields = document.getElementById("additionalFields");
  
      this.openModalBtn.addEventListener("click", this.openModal.bind(this));
      this.closeBtns.forEach(btn => btn.addEventListener("click", this.closeModal.bind(this)));
      this.createBtn.addEventListener("click", this.createVisit.bind(this));
      this.doctorDropdown = document.getElementById("doctor");
      this.doctorDropdown.addEventListener("change", this.updateAdditionalFields.bind(this));
    }
  
    openModal() {
      this.modal.style.display = "block";
    }
  
    closeModal() {
      this.modal.style.display = "none";
    }
  
    updateAdditionalFields() {
      const selectedDoctor = this.doctorDropdown.value;
      this.additionalFields.innerHTML = "";
  
      if (selectedDoctor === "Cardiologist") {
        this.additionalFields.innerHTML = `
          <label for="vascularPressure">Vascular Pressure:</label>
          <input type="text" id="vascularPressure" name="vascularPressure">
          <label for="bodyMassIndex">Body Mass Index:</label>
          <input type="text" id="bodyMassIndex" name="bodyMassIndex">
          <label for="cardioDiseases">Diseases of the Cardiovascular System:</label>
          <input type="text" id="cardioDiseases" name="cardioDiseases">
          <label for="age">Age:</label>
          <input type="text" id="age" name="age">
        `;
      } else if (selectedDoctor === "Dentist") {
        this.additionalFields.innerHTML = `
          <label for="lastVisitDate">Last Visit Date:</label>
          <input type="date" id="lastVisitDate" name="lastVisitDate">
        `;
      } else if (selectedDoctor === "Therapist") {
        this.additionalFields.innerHTML = `
          <label for="age">Age:</label>
          <input type="text" id="age" name="age">
        `;
      }
    }
  
    createVisit() {
      // Implement AJAX POST request here and create Visit object
      // Then close the modal
      this.closeModal();
    }
  }
  
  class Visit {
    constructor(data) {
      this.fullname = data.fullname;
      this.doctor = data.doctor;
      this.purpose = data.purpose;
      this.description = data.description;
      this.urgency = data.urgency;
    }
  }
  
  class VisitCardiologist extends Visit {
    constructor(data) {
      super(data);
      this.vascularPressure = data.vascularPressure;
      this.bodyMassIndex = data.bodyMassIndex;
      this.cardioDiseases = data.cardioDiseases;
      this.age = data.age;
    }
  }
  
  class VisitDentist extends Visit {
    constructor(data) {
      super(data);
      this.lastVisitDate = data.lastVisitDate;
    }
  }
  
  class VisitTherapist extends Visit {
    constructor(data) {
      super(data);
      this.age = data.age;
    }
  }
  
  const modal = new Modal();
  