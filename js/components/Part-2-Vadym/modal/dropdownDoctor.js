import createCard from "./createCard.js";

// const docList = document.querySelectorAll('.doc');
// const btnDoc = document.querySelector('.specialist');
// const additionalFields = document.querySelectorAll('.additional-fields');

 // Знаходимо всі елементи випадаючого меню з класом "dropdown-item"

 

export default function dropdownDoctor() {

// Знаходимо всі елементи випадаючого меню з класом "dropdown-item"
let dropdownItems = document.querySelectorAll('.dropdown-item');

// Знаходимо кнопку, до якої потрібно буде додати назву обраного спеціаліста
let specialistButton = document.querySelector('.specialist');

// Знаходимо всі додаткові поля
let additionalFields = document.querySelectorAll('.additional-fields');

// Додаємо обробник події 'click' до кожного елемента випадаючого меню
dropdownItems.forEach(function(item) {
    item.addEventListener('click', function(event) {
        // Зчитуємо текст обраного елемента
        let selectedSpecialist = event.target.textContent;
     

        // Додаємо назву обраного спеціаліста до тексту кнопки
        specialistButton.textContent = selectedSpecialist;
        
        // Приховуємо всі додаткові поля, видаляючи клас "active"
        additionalFields.forEach(function(field) {
            field.classList.remove('active');
        });
        
        // Показуємо тільки ті додаткові поля, які відповідають вибраному спеціалісту
        // Для цього додаємо клас "active" до відповідних полів
        let relatedFields = document.querySelectorAll(".additional-fields");
        relatedFields.forEach(function(field) { field?.classList.contains(event.target)
            field.classList.add('active');
        });
        
        // Щоб сторінка не перезавантажувалась після кліку по посиланню
        event.preventDefault();
    });
});


}

       

        
        // docList.forEach(item => {
        //     item.classList.remove('active');  // Remove ".active" from all items
        // });
        // item.classList.add('active');         // Add ".active" to the clicked item
        // btn.textContent = item.textContent;

        // let selectedDocType = document.querySelector(".doc.active");
        // console.log(selectedDocType.classList[1]);
        // createCard(selectedDocType.classList[1]);
