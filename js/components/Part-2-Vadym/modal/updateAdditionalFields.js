updateAdditionalFields() {
    const selectedDoctor = this.doctorDropdown.value;
    this.additionalFields.innerHTML = "";

    if (selectedDoctor === "Cardiologist") {
      this.additionalFields.innerHTML = `
        <label for="vascularPressure">Vascular Pressure:</label>
        <input type="text" id="vascularPressure" name="vascularPressure">
        <label for="bodyMassIndex">Body Mass Index:</label>
        <input type="text" id="bodyMassIndex" name="bodyMassIndex">
        <label for="cardioDiseases">Diseases of the Cardiovascular System:</label>
        <input type="text" id="cardioDiseases" name="cardioDiseases">
        <label for="age">Age:</label>
        <input type="text" id="age" name="age">
      `;
    } else if (selectedDoctor === "Dentist") {
      this.additionalFields.innerHTML = `
        <label for="lastVisitDate">Last Visit Date:</label>
        <input type="date" id="lastVisitDate" name="lastVisitDate">
      `;
    } else if (selectedDoctor === "Therapist") {
      this.additionalFields.innerHTML = `
        <label for="age">Age:</label>
        <input type="text" id="age" name="age">
      `;
    }
  }