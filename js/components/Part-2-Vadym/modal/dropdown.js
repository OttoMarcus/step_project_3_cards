export default function dropdown(args, btn) {
    args.forEach(item => {
        item.addEventListener('click', function() {     //change dropdown
            btn.textContent = item.textContent;
        });
    })


const additionalFields = document.querySelectorAll('.additional-fields');
const doctors = document.querySelector(".doctors");

args.forEach(item => {
    item.addEventListener('click', function() {
        const selectedClass = item.classList[1];        // Get the second class from list of doctors

        additionalFields.forEach(field => {
            field.classList.remove('active');     // Remove "active" class from all fields
            field.querySelector('input').removeAttribute('required');
        });

        additionalFields.forEach(field => {
            if (field.classList.contains(selectedClass)) {
                field.classList.add('active');               // Add ".active" to fields with the matching class of doctors
                field.querySelector('input').setAttribute('required', true);
            }
        });

        args.forEach(item => {
            item.classList.remove('active');  // Remove ".active" from all items
        });
        item.classList.add('active');         // Add ".active" to the clicked item
        btn.textContent = item.textContent;

        let selectedDocType = doctors.querySelector(".active");
        // console.log(selectedDocType.classList[1]);
        // createCard(selectedDocType.classList[1]);
    });
});

}