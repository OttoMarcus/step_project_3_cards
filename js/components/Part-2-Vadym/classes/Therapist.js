import Visit from "./Visit.js";
export default class Therapist extends Visit {
    constructor(firstname, lastname, purpose, description, doctor, urgency, age) {
        super(firstname, lastname, purpose, description, doctor, urgency);
        this.age = age;
        }
}
