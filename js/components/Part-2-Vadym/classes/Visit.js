export default class Visit {
    constructor(firstname, lastname, purpose, description, doctor, urgency) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.purpose = purpose;
        this.description = description;
        this.doctor = doctor;
        this.urgency = urgency;

    }
}