import Visit from "./Visit.js";
export default class Cardiologist extends Visit {
    constructor(firstname, lastname, purpose, description, doctor, urgency, vascularPressure, bodyMassIndex, cardioDiseases, age) {
        super(firstname, lastname, purpose, description, doctor, urgency);
        this.vascularPressure = vascularPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.cardioDiseases = cardioDiseases;
        this.age = age;
    }
}
