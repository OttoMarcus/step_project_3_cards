import Visit from "./Visit.js";
export default class Dentist extends Visit {
    constructor(firstname, lastname, purpose, description, doctor, urgency, lastVisit) {
        super(firstname, lastname, purpose, description, doctor, urgency);
        this.lastVisit = lastVisit;
    }
}
